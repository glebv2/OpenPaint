# SPDX-License-Identifier: GPL-2.0-or-later

# by gleb maintener

from turtle import *
from config import *

paint = Turtle()
paint.color(color)
paint.shape(shape)
paint.speed(speed)
paint.goto(xy)
paint.width(width)
print("start OK")

def draw(x, y):
	paint.goto(x, y)

def move(x, y):
	paint.penup()
	paint.goto(x, y)
	paint.pendown()

def color_red():
	paint.color("red")

def color_blue():
	paint.color("blue")

def color_green():
	paint.color("green")

def color_black():
	paint.color("black")

def hide():
	paint.hideturtle()

def show():
	paint.showturtle()

def Up():
        paint.goto(paint.xcor(), paint.ycor()+10)
def Down():
        paint.goto(paint.xcor(), paint.ycor()-10)
def Left():
        paint.goto(paint.xcor()-10, paint.ycor())
def Right():
        paint.goto(paint.xcor()+10, paint.ycor())

def clear():
	paint.clear()
	paint.penup()
	paint.goto(0,0)
	paint.pendown()

def up_w():
	paint.goto(paint.xcor(), paint.ycor()+10)

def down_s():
	paint.goto(paint.xcor(), paint.ycor()-10)

def right_d():
	paint.goto(paint.xcor()+10, paint.ycor())

def left_a():
	paint.goto(paint.xcor()-10, paint.ycor())

def left_q():
	paint.goto(paint.xcor()+10, paint.ycor())

def right_e():
	paint.goto(paint.xcor()+10, paint.ycor())

paint.ondrag(draw)
target = paint.getscreen()
target.onscreenclick(move)
target.listen()

target.onkey(show, "o")
target.onkey(hide, "h")
target.onkey(color_red, "r")
target.onkey(color_blue, "u")
target.onkey(color_green, "g")
target.onkey(color_black, "b")
target.onkey(Up, "Up")
target.onkey(Down, "Down")
target.onkey(Right, "Right")
target.onkey(Left, "Left")
target.onkey(left_a, "a")
target.onkey(right_d, "d")
target.onkey(down_s, "s")
target.onkey(up_w, "w")
target.onkey(right_e, "e")
target.onkey(left_q, "q")
target.onkey(clear, "c")

mainloop()
